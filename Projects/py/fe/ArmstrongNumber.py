import math


def is_armstrong_number(num):
    if 99 < num < 1000:
        temp = num
        total = 0
        while temp > 0:
            rem = temp % 10
            temp = temp / 10
            total += int(math.pow(rem, 3))

        print(total)
        if total == num:
            return True

    return False


if __name__ == "__main__":
    result = is_armstrong_number(int(input("Enter a number:")))
    if result:
        print("Input number is  an Armstrong number")
    else:
        print("Input number is not an armstrong number")
