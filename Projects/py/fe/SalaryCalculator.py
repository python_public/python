class Salary:
    def __init__(self):
        self.basic = float(input("Enter basic pay of employee:"))
        self.hra = (10/100)*self.basic
        self.ta = (5/100)*self.basic
        self.tax = 2;

    def calculate_gross_salary(self):
        return (self.basic + self.hra + self.ta)

    def calculate_net_salary(self):
        gross_salary = self.calculate_gross_salary()
        net_salary = gross_salary - ((self.tax/100)*gross_salary)
        return net_salary


def main():
    salary = Salary();
    print("Gross salary of employee:",salary.calculate_gross_salary())
    print("Net salary of employee:",salary.calculate_net_salary())


if __name__ == '__main__':
    main()

