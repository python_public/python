import math


def binary_to_int(binary_input):
    num = 0
    power_of_two = 0
    for i in range(len(binary_input) - 1, -1, -1):
        num += int(binary_input[i]) * math.pow(2, power_of_two)
        power_of_two += 1

    return num


print("Decimal number:", binary_to_int(input("Enter a binary number:")))
